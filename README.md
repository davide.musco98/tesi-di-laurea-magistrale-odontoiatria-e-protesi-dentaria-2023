# Comparazione sugli effetti della somministrazione di Etoricoxib e Betametasone sulla qualità della vita correlata alla salute orale e sulla gestione delle sequele postoperatorie associate a estrazione chirurgica del terzo molare mandibolare: uno studio pilota

Questa tesi è stata realizzata da Davide Musco (davide.musco98@gmail.com) ed è disponibile sotto licenza Creative Commons Attribution-ShareAlike 4.0 International.
