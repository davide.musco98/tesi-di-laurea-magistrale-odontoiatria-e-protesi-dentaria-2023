NAME = tesi
LATEX = lualatex
BIBTEX = biber
INKSCAPE = flatpak run org.inkscape.Inkscape

%.pdf: %.tex %.bib
	$(LATEX) -halt-on-error -interaction=nonstopmode $<
	$(BIBTEX) $(NAME)
	$(LATEX) -halt-on-error -interaction=nonstopmode $<
	
%.svg.pdf: %.svg
	$(INKSCAPE) -D $< -o $@ --export-latex

all: img/winter.svg.pdf img/tri.svg.pdf img/ede.svg.pdf img/vas.svg.pdf img/logo.svg.pdf img/consenso.svg.pdf img/fans.svg.pdf img/edema.svg.pdf img/bentela.svg.pdf img/pelltot.svg.pdf img/tac.jpg img/endorx.jpg img/panorx.jpg $(NAME).pdf

presentation: presentazione/presentazione.tex
	$(LATEX) -halt-on-error -interaction=nonstopmode $<

clean:
	rm -f $(NAME).pdf $(NAME).aux $(NAME).log $(NAME).out $(NAME).toc *.bbl *.blg *.bcf img/*.svg.pdf img/*.svg.pdf_tex

docx:
	pandoc $(NAME).tex -o $(NAME).docx

.PHONY: all clean docx
